﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FullscreenImage : MonoBehaviour {
	[SerializeField] bool executeOnUpdate, keepAspectRatio;
	SpriteRenderer sr;

	void Awake() {
		sr = GetComponent<SpriteRenderer>();
		Resize ();
	}

	void Update(){
		if (executeOnUpdate)
			Resize ();
	}

	public void Resize(){
		transform.localScale = new Vector3(1f, 1f, 1f);

		// example of a 640x480 sprite
		float width = sr.sprite.bounds.size.x; // 4.80f
		float height = sr.sprite.bounds.size.y; // 6.40f

		// and a 2D camera at 0,0,-10
		float worldScreenHeight = Camera.main.orthographicSize * 2f; // 10f
		float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width; // 10f

		Vector3 imgScale = new Vector3(1f, 1f, 1f);

		// do we scale according to the image, or do we stretch it?
		if (keepAspectRatio){
			Vector2 ratio = new Vector2(width / height, height / width);
			if ((worldScreenWidth / width) > (worldScreenHeight / height)){
				// wider than tall
				imgScale.x = worldScreenWidth / width;
				imgScale.y = imgScale.x * ratio.y;

				Debug.Log ("wider than tall");
			}
			else{
				// taller than wide
				imgScale.y = worldScreenHeight / height;
				imgScale.x = imgScale.y * ratio.x;      

				Debug.Log ("taller than wide, imgScale.x=" + imgScale.x + ", imgScale.y=" + imgScale.y + ", ratio=" + ratio.x + ":" + ratio.y);      
			}
		}
		else {
			imgScale.x = worldScreenWidth / width;
			imgScale.y = worldScreenHeight / height;
		}

		// apply change
		transform.localScale = imgScale;
	}
}
