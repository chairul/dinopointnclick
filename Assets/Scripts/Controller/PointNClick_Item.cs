﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PointNClick_Item : MonoBehaviour, IPointerClickHandler {

	public Dinosaurus dino;

	public void Create(string name, string desc, string spr) {
		dino = new Dinosaurus (name, desc, this, spr);
	}

	public void OnPointerClick(PointerEventData eventData){
		PointNClick_Controller.instance.ShowPopup (dino);
	}
}