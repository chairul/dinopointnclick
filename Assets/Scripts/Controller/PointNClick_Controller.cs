﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class PointNClick_Controller : MonoBehaviour {

	public static PointNClick_Controller instance;

	[SerializeField] GameObject popup, textName, textDesc, imageDino;
	[SerializeField] SpriteRenderer background;
	[SerializeField] List<GameObject> DinosaurusList;
	[SerializeField] List<Sprite> BackgroundList;
	Dictionary<string,GameObject> Dinos;

	List<GameObject> DinoInThisScene = new List<GameObject> ();
	int curLayoutIndex = 0;

	JSONNode JSONLoaded;
	PointNClick_Item currentDino;

	void Awake() {
		instance = this;

		Dinos = new Dictionary<string,GameObject> ();
		foreach (GameObject go in DinosaurusList) {
			Dinos.Add (go.name, go);
		}
	}

	void Start () {
		LoadJSON ("data/pnc");
	}

	void LoadJSON(string url){
		string json = Resources.Load<TextAsset>(url).text;
		OnLoadJSON (json);
	}

	void OnLoadJSON(string json){
		JSONLoaded = JSON.Parse(json);

		SetLayoutContent (0);
	}

	public void ShowPopup(Dinosaurus dino){
		SetPopupContent (dino);

		popup.transform.parent.gameObject.SetActive (true);
	}

	public void HidePopup(){
		popup.transform.parent.gameObject.SetActive (false);
	}

	public void NextLayout(){
		if (curLayoutIndex < JSONLoaded ["PointNClick"].Count - 1)
			curLayoutIndex++;
		else
			curLayoutIndex = 0;

		SetLayoutContent (curLayoutIndex);
	}

	public void PrevLayout(){
		if (curLayoutIndex > 0)
			curLayoutIndex--;
		else
			curLayoutIndex = JSONLoaded ["PointNClick"].Count - 1;

		SetLayoutContent (curLayoutIndex);
	}

	public void SetPopupContent(Dinosaurus dino){
		currentDino = dino.Item;
		textName.GetComponent<Text>().text = dino.Name;
		textDesc.GetComponent<Text>().text = dino.Desc;

		Sprite dinoImage = Resources.Load<Sprite> (dino.PopupImage);
		imageDino.GetComponent<Image> ().sprite = dinoImage;
	}

	void SetLayoutContent(int i){
		ClearPreviousDino ();

		JSONNode curNode = JSONLoaded ["PointNClick"] [i];
		LoadBackground (i);

		for (int index = 0; index < curNode ["dinos"].Count; index++) {
			string dinoName = curNode ["dinos"] [index] ["name"];
			string dinoDesc = curNode ["dinos"] [index] ["desc"];
			Vector2 dinoPos = new Vector2 (curNode ["dinos"] [index] ["position"] ["x"].AsFloat, curNode ["dinos"] [index] ["position"] ["y"].AsFloat);
			float dinoRotation = curNode ["dinos"] [index] ["rotation"] ["z"].AsFloat;
			Vector2 dinoScale = new Vector2 (curNode ["dinos"] [index] ["scale"] ["x"].AsFloat, curNode ["dinos"] [index] ["scale"] ["y"].AsFloat);
			string dinoImage = "Sprites/DinosaurThumbnail/puzzle_pic_" + dinoName;

			GameObject instantiated = Instantiate (Dinos ["Dino " + dinoName]);
			instantiated.transform.position = dinoPos;
			instantiated.transform.rotation = Quaternion.Euler(0, 0, dinoRotation);
			instantiated.transform.localScale = dinoScale;
			instantiated.AddComponent <PointNClick_Item> ().Create (dinoName, dinoDesc, dinoImage);

			DinoInThisScene.Add (instantiated);
		}
	}

	void ClearPreviousDino(){
		if (DinoInThisScene.Count > 0) {
			foreach (GameObject go in DinoInThisScene) {
				Destroy (GameObject.Find (go.name));
			}
		}
		DinoInThisScene.Clear ();
	}

	void LoadBackground(int i){
		background.sprite = BackgroundList [i];
		background.GetComponent<FullscreenImage> ().Resize ();
	}

	public void NextDino(){
		var curIndex = DinoInThisScene.IndexOf (currentDino.gameObject);

		if (curIndex < DinoInThisScene.Count - 1) {
			curIndex++;
		} else {
			curIndex = 0;
		}

		Dinosaurus nextDino = DinoInThisScene [curIndex].GetComponent<PointNClick_Item> ().dino;
		SetPopupContent (nextDino);
	}

	public void PrevDino(){
		var curIndex = DinoInThisScene.IndexOf (currentDino.gameObject);

		if (curIndex > 0) {
			curIndex--;
		} else {
			curIndex = DinoInThisScene.Count - 1;
		}

		Dinosaurus nextDino = DinoInThisScene [curIndex].GetComponent<PointNClick_Item> ().dino;
		SetPopupContent (nextDino);
	}
}
