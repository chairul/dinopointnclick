﻿using UnityEngine;

public class Dinosaurus {

	public string Name{ get; set; }
	public string Desc{ get; set; }

	public string PopupImage { get; set; }
	public PointNClick_Item Item { get; set; }

	public Vector2 Position { get; set; }
	public Vector2 Scale { get; set; }

	public Dinosaurus(string dinoName, string dinoDesc, PointNClick_Item item, string dinoImage){
		this.Name = dinoName;
		this.Desc = dinoDesc;
		this.Item = item;
		this.PopupImage = dinoImage;
	}
}